# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from setuptools import find_packages, setup

setup(name='TerrainLib',
      version='0.0.1a0',
      description='Libraries of simple to use, realistically looking landscapes.',
      author='Nathan Graule',
      author_email='solarliner@gmail.com',
      url='https://www.python.org/community/sigs/',
      packages=find_packages(exclude=["*_test", "tests"]),
      install_requires=["numpy>=1.15", "Pillow>=5.3", "numexpr>=2.6", "opensimplex>=0.2"],
      test_suite="tests.test_suite"
      )

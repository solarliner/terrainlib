===============
Getting Started
===============

Installation
------------

TerrainLib is available on PyPi, therefore the easiest way to install it is the following

.. code-block:: bash

    pip install TerrainLib

**Note**: As of version 0.0.2a the library is not yet available on PyPi to focus on quicker development of the library.

You can also install the library by downloading a version over the `GitLab project home`_.

.. _GitLab project home: https://gitlab.com/solarliner/terrainlib


Introduction to the library
---------------------------
Once the library installed, you can import the algorithms from those three places:

* :code:`terrainlib.generator` holds all generators, in sub-categories

* :code:`terrainlib.filters` holds all filters, also in sub-categories

* :code:`terrainlib.readers` holds all readers, in, you guessed it, sub-categories.

* :code:`terrainlib.runners` take a list of all three above, and run them following some predefined rule(s). An example
  is the list runner which runs them sequentially.

* :code:`terrainlib.operators` get a list of Terrain objects and process them to only output one. They are useful in
  cases where several inputs need to merge together into one, or for graph-like data flows.

Definitions
~~~~~~~~~~~

What do we mean by those words? It may sound weird, as I, a non-native English speaker chose those words, but it allows
everyone to know what they're talking about. Each one of those are generally called *Transforms*:

Generator
  An algorithm that only has parameter inputs, and outputs a terrain. This include image import, Perlin noise, etc.

Filter
  An algorithm that both inputs and outputs a terrain, while also accepting other parameter inputs. Those are erosion
  algorithms, manipulations on the terrain, masking, etc.

Reader
  An algorithm that inputs a terrain (and parameters) and output something completely different. These include image
  export, mesh export, texture output, 'playability' scores, etc.


By feeding each the output of the previous algorithm we are able to define *pipelines* that define our terrain.
Additionaly, there are *Operators* which take several terrains and output only one, where several terrains might merge
into one.

With those definitions out of the way, let's create a Terrain right now!

How to use the library
----------------------

TerrainLib's primary usage for artists is by scripting. Writing lines of code directly, instead of using a modifier
system or a graph, allows both the artist to handle data how he sees fit, and developers that want to integrate
TerrainLib into other programs that would fit another workflow (node editors, modifier stacks, etc.). This way there are
no restrictions at all on how the landscape is generated.

Unfortunately, TerrainLib requires that artists write Python code to effectively use the library. Fortunately, though,
the library is easy enough to use that it will only take a couple of minutes to hours to get used to the new workflow -
while also hopefully spiking a curiosity for programming.

The library was designed with a particular data flow in mind. Generators create the Terrain object, which is then
transformed through a series of filters, before getting exported to one or several readers - like saving the landscape
as a heightfield and a mesh, or displaying several metrics about the terrain...

An example pipeline can be visualized by the following graph:

.. mermaid::
  :alt: Example data flow graph
  :align: center
  :caption: Example data flow graph

  graph TD;
    DiamondSquareGenerator --> Multiply                                                                                                                                                      
    VoronoiGenerator --> Normalize                                                                                                                                                           
    Normalize --> Multiply
    Multiply-->TerraceErosionFilter
    TerraceErosionFilter-->HydraulicErosionFilter
    HydraulicErosionFilter-->PlayabilityScoreReader
    HydraulicErosionFilter-->MeshReader

The above graph can be described by the following script:

.. code-block:: python
  :linenos:

  import numpy
  # The DiamondSquareGenerator and VoronoiGenerator are two Python classes that generate
  # a terrain.
  from terrainlib.generators.procedural import DiamondSquareGenerator, VoronoiGenerator
  # The HydraulicErosionFilter, as well as the SmoothTerraceErosionFilter, are two filters
  # that will modify an existing terrain.
  from terrainlib.filters.erosion import HydraulicErosionFilter, SmoothTerraceErosionFilter
  # The PlayabilityScoreReader is a (as of time of writing) fictional class that
  # analyzes the terrain to derive a metric, here a playability score.
  from terrainlib.readers import PlayabilityScoreReader
  # The MeshReader is (also as of time of writing) a fictitious class that outputs a
  # mesh file, to import in your favorite 3D program.
  from terrainlib.readers.mesh import MeshReader

  # Here we setup the different transforms to be used later on. Transforms in TerrainLib are
  # first initialized, and then used (in Python, this results in first initializing the class
  # before calling it with the Terrain object.
  gen_ds = DiamondSquareGenerator(12, 0.1)
  gen_vo = VoronoiGenerator(4097)
  filt_hydro = HydraulicErosionFilter(350)
  filt_terrace = TerraceErosionFilter(0.2)
  read_play = PlayabilityScoreReader()
  read_mesh = MeshReader('obj', 'grid')

  # The Voronoi generator needs a list of points. The function returns a list of 50 2D points.
  points = numpy.random.uniform(0, 4096, (50, 2))

  # We finally get to generating the terrain! We generate the terrain, and transform it
  # the way we want, getting a Terrain object in the end that we can export through the
  # reader(s) of our choice.
  terrain = gen_ds() * gen_vo().to_normalized_terrain()
  terrain = filt_hydro(filt_terrace())
  
  playability = read_play(terrain)
  mesh_data = read_mesh(terrain)

**Note**: This is an idealized version of the script as all functionality is not included yet. It might also change as development continues.

At the end of this tutorial, hopefully everything about this script will make sense!

A first pipeline
~~~~~~~~~~~~~~~~

Start off by importing the Diamond Square generator. This is a fast noise generation algorithm that can output detailed
terrain fast - at the cost of not being tileable.

.. code-block:: python
   :linenos:

    from terrainlib.generators.procedural import DiamondSquareGenerator

The output of that generator will be either too flat or too rough for the terrain to be interesting or even somewhat
realistic. To smooth things out, we need to simulate the process by which rocks and dirt fall downhill.  
We need some thermal erosion:

.. code-block:: python
   :linenos:
   :lineno-start: 2

    from terrainlib.filters.erosion import ThermalErosionFilter

Last, but not least, we need to export that terrain somewhere. The best way to do that is through an image - it's easily
visualizable, and can be imported anywhere really. Load up the Image exporter:

.. code-block:: python
   :linenos:
   :lineno-start: 3

    from terrainlib.readers.image import PILImageReader

Now, we need to initialize those. We're going to create a 1025 pixel wide terrain, with a decent amount of roughness,
then apply 150 iterations of erosion at standard rates, and then export it to 'terrain_out.png' as a 16-bit image.

.. code-block:: python
   :linenos:
   :lineno-start: 4

    generator = DiamondSquareGenerator(10, 0.1)
    erosion = ThermalErosionFilter(150)
    img_reader = PILImageReader(PILImageReader.BITDEPTH_16)

Couple of things to notice:

* Due to the way the Diamond Square algorithm works, we do not enter the desired size directly, but the power of two
  that will result in the desired size. Here, we're taking the 10th power of two (2^10 = 1024), and the algorithm adds
  one to that number (this is a technical restriction, the algorithm needs a center pixel to work with, needing an odd
  number of pixels on the side). Thus, we get a 1025 pixel wide terrain.

* We aren't providing a filename to the reader *directly*, because the reader outputs a Pillow image. The actual file
  will be saved from the `PIL.Image` instance.

Now, let's setup our pipeline:

.. code-block:: python
   :linenos:
   :lineno-start: 9

    terrain = erosion(generator())
    img = img_reader(terrain)
    img.save('terrain_out.png')

If you run your script, and after some processing time, it will have created a file named 'terrain_out.png' with the 
terrain saved in it. If you open that image as a heightfield, you will see your image in all its glory!

Here is the whole script:

.. code-block:: python
   :linenos:

    from terrainlib.generators.procedural import DiamondSquareGenerator
    from terrainlib.filters.erosion import ThermalErosionFilter
    from terrainlib.readers.image import PILImageReader

    generator = DiamondSquareGenerator(10, 0.1)
    erosion = ThermalErosionFilter(150)
    img_reader = PILImageReader(PILImageReader.BITDEPTH_16)

    terrain = erosion(generator())
    img = img_reader(terrain)
    img.save('terrain_out.png')

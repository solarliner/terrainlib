"""Lambda based operators that allow an arbitrary number of terrains to be merged."""
# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from terrainlib.operators.base import TerrainOperator, TerrainOperatorCallable
from terrainlib.terrain import Terrain


class FunctionTerrainOperator(TerrainOperator):

    def __init__(self, operator_function: TerrainOperatorCallable):
        self.func = operator_function

    def __call__(self, *terrains: Terrain):
        return self.func(*terrains)


def function_operator(func: TerrainOperatorCallable):
    return FunctionTerrainOperator(func)


__all__ = ["FunctionTerrainOperator", "function_operator"]

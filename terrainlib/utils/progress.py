"""Progress report utility code"""

# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
from typing import Any, Callable, List


class ProgressReporterMixin:

    def __init__(self):
        self.step = 0
        self.steps = 0
        self.listeners = []  # type: List[Callable[[int, int], Any]]

    @property
    def progress(self):
        if self.steps is None or self.steps <= 0:
            return None
        return self.step / self.steps

    def add_progress_listener(self, listener: Callable[[int, int], Any]):
        self.listeners.append(listener)

    def _update_progress(self, step: int = None, steps: int = None):
        if step is not None:
            self.step = max(0, step)
        if steps is not None:
            self.steps = max(0, steps)
        for listener in self.listeners:
            listener(self.step, self.steps)

    def _step(self):
        self._update_progress(step=self.step + 1)


class LoggerProgressListener:

    def __init__(self, reporter: ProgressReporterMixin):
        self.logger = logging.getLogger(reporter.__class__.__name__)
        self.reporter = reporter
        reporter.add_progress_listener(self.progress_report)

    def progress_report(self, step: int, steps: int):
        prog_report = "%.2f%% - Step %i / %i" % (100 * self.reporter.progress, step, steps)
        print(prog_report + "\r")


__all__ = ["ProgressReporterMixin", "LoggerProgressListener"]

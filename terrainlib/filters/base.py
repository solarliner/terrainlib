"""Abstract class definitions to setup the convention of Terrain Filters."""
# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from typing import Callable

from ..terrain import Terrain


class TerrainFilter(Callable[[Terrain], Terrain]):
    """Modifies a Terrain object."""

    def __call__(self, terrain: Terrain) -> Terrain:
        """Apply transform onto the terrain. Must output terrain afterwards."""
        raise NotImplementedError()


__all__ = ["TerrainFilter"]

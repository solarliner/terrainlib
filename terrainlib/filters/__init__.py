"""Terrain filters apply transformation onto a terrain. Masking, eroding, sculpting... You're at the right place!"""
# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import numpy

from ..terrain import Terrain
from .base import TerrainFilter


class ScaleHeightsFilter(TerrainFilter):
    def __init__(self, min_height: float, max_height: float):
        self.min = min_height
        self.max = max_height

    def __call__(self, terrain: Terrain):
        arr = terrain.to_normalized_array()
        scaled_arr = numpy.add(self.min, numpy.multiply(self.max - self.min, arr))
        return Terrain(array=scaled_arr)


__all__ = ["ScaleHeightsFilter"]

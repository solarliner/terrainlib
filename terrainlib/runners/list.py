import logging
from collections import OrderedDict
from typing import Any, Dict, Optional

from terrainlib.filters.base import TerrainFilter
from terrainlib.generators.base import TerrainGenerator
from terrainlib.runners import BaseTerrainRunner, TerrainTransform
from terrainlib.terrain import Terrain
from terrainlib.utils.progress import ProgressReporterMixin

logger = logging.getLogger(__name__)


class ListTerrainRunner(BaseTerrainRunner, TerrainFilter, ProgressReporterMixin):

    def __init__(self, transforms: Dict[Any, TerrainTransform]):
        # FIXME: Specify transforms being an OrderedDict
        ProgressReporterMixin.__init__(self)
        if not isinstance(transforms, OrderedDict):
            error_text = "Transforms must be an ordered dictionary as it needs an explicit order of transforms to run"
            logger.error(error_text)
            raise ValueError(error_text)
        self.transforms = transforms
        self.results = dict()
        for transform in self.transforms.values():
            if isinstance(transform, ProgressReporterMixin):
                self._update_progress(steps=self.steps + transform.steps)
            else:
                self._update_progress(steps=self.steps + 100)

    def run(self, terrain: Optional[Terrain] = None):
        if terrain is None:
            actual_terrain = None
        else:
            actual_terrain = terrain.copy()
        for i, transform in self.transforms.items():  # type: TerrainTransform
            if isinstance(transform, ProgressReporterMixin):
                transform.add_progress_listener(self.transform_progress_hook)
            else:
                self._update_progress(step=self.step + 100)

            logger.debug("Running transform %s", repr(transform.__class__))
            if isinstance(transform, TerrainGenerator):
                actual_terrain = transform()
            else:
                # noinspection PyCallingNonCallable
                actual_terrain = transform(actual_terrain)

        return actual_terrain

    def __call__(self, terrain: Terrain):
        return self.run(terrain)

    def transform_progress_hook(self, step, steps):
        self._step()


__all__ = ["ListTerrainRunner"]

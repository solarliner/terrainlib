from typing import Optional, Union

from terrainlib.filters.base import TerrainFilter
from terrainlib.generators.base import TerrainGenerator
from terrainlib.readers.base import TerrainReader
from terrainlib.terrain import Terrain


class BaseTerrainRunner:

    def run(self, terrain: Optional[Terrain] = None):
        raise NotImplementedError()


TerrainTransform = Union[TerrainGenerator, TerrainFilter, TerrainReader]


__all__ = ["BaseTerrainRunner", "TerrainTransform"]

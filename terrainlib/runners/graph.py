import logging
import multiprocessing as mp
import sys
import warnings
from collections import OrderedDict
from queue import Queue
from typing import Any, Dict, List, Optional, Sequence, Tuple

from terrainlib.filters.base import TerrainFilter
from terrainlib.generators.base import TerrainGenerator
from terrainlib.runners import BaseTerrainRunner, TerrainTransform
from terrainlib.terrain import Terrain

TerrainOperator = Any

MP_PROCESS_METHOD_SET = False

logger = logging.getLogger(__name__)


def warning(reason: str):
    def warning_decorator(cls):
        class WarnedClass(cls):
            def __init__(self, *args, **kwargs):
                warnings.simplefilter("always", Warning)
                warnings.warn(reason.format(cls=cls.__name__))
                cls.__init__(self, *args, **kwargs)

        return WarnedClass

    return warning_decorator


def set_process_method():
    global MP_PROCESS_METHOD_SET
    if not MP_PROCESS_METHOD_SET:
        logger.info("Setting multiprocessing start method")
        if sys.platform.startswith('windows'):
            mp.set_start_method('spawn')
        else:
            mp.set_start_method('fork')
    MP_PROCESS_METHOD_SET = True


def _runner(transform: TerrainTransform, terrain: Optional[Terrain] = None):
    if isinstance(transform, TerrainGenerator):
        result = transform()
    else:
        if terrain is not None:
            result = transform(terrain)
        else:
            raise ValueError('Terrain cannot be None when the transform is not a generator.')
    return result


@warning(
    "{cls} is not stable - this runner never returns. See https://gitlab.com/solarliner/terrainlib/issues/24 for "
    "more details"
)
class GraphTerrainRunner(BaseTerrainRunner, TerrainFilter):

    def __init__(self, transforms_node: Sequence[TerrainTransform], operator: TerrainOperator):
        # TODO: Implement TerrainOperator
        set_process_method()
        self.nodes = list(enumerate(transforms_node))
        self.Q = Queue()
        self.results_queue = mp.Queue()
        self.operator = operator

    def run(self, terrain: Optional[Terrain] = None) -> Terrain:
        processes = []  # type: List[mp.Process]
        for item in self.nodes:
            self.Q.put(item)
        with mp.Manager() as manager:
            d = manager.dict()
            logger.info("Spawning %i daemon processes", mp.cpu_count())
            for _ in range(mp.cpu_count()):
                p = mp.Process(target=self._worker, args=(d,), name=self.__class__.__name__)
                p.daemon = True
                p.start()
                processes.append(p)
                self.Q.put(None)
            self.Q.join()
            results = OrderedDict(d)
            return self.operator(*results.values())

    def join(self):
        self.Q.join()

    def __call__(self, terrain: Optional[Terrain] = None):
        return self.run(terrain)

    def _worker(self, results: Dict[int, TerrainTransform]):
        logger.debug("Worker started")
        while not self.Q.empty():
            item = self.Q.get()  # type: Tuple[int, TerrainTransform]
            logger.info("Got item")
            logger.debug("Item: %s", repr(item))
            if item is None:
                self.Q.task_done()
                break
            i, transform = item
            terrain = _runner(transform)
            results[i] = terrain
            self.Q.task_done()
        logger.debug("Worker finished")


__all__ = ["GraphTerrainRunner"]

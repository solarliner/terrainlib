from itertools import product
import unittest

import numpy

from terrainlib.terrain import Terrain


class TerrainTestCase(unittest.TestCase):
    def test_accepts_size(self):
        terr = Terrain(size=256)

        self.assertEqual(terr.size, 256)

    def test_accepts_array(self):
        array = numpy.random.uniform(size=(256, 256))
        terr = Terrain(array)

        self.assertEqual(terr.size, 256)
        self.assertTrue(numpy.equal(terr._heightmap, array).all())
        assert numpy.equal(terr._heightmap, array).all()

    def test_throws_on_nonsquare_array(self):
        with self.assertRaises(TypeError):
            Terrain(array=numpy.random.uniform(size=(256, 128)))

    def test_throws_on_non_2d_array(self):
        with self.assertRaises(TypeError):
            Terrain(array=numpy.random.uniform(size=128))

    @unittest.expectedFailure
    def test_get_item_int(self):
        terr = Terrain(size=64)

        self.assertEqual(terr[0], numpy.zeros(64))

    @unittest.expectedFailure
    def test_get_item_float(self):
        arr = numpy.zeros((64, 64))
        arr[::2] = numpy.ones(64)

        terr = Terrain(array=arr)
        self.assertEqual(terr[0.5], numpy.ones(64) * 0.5)

    def test_get_item_tuple(self):
        terr = Terrain(array=numpy.ones((64, 64)))
        self.assertEqual(terr[0, 0], 1)

    @unittest.expectedFailure
    def test_get_item_slice(self):
        arr = numpy.arange(0, 64).reshape((16, 16))
        terr = Terrain(array=arr)

        self.assertEqual(terr[::2], arr[::2])
        self.assertEqual(terr[1:4, 1:4], arr[1:4, 1:4])

    def test_terrain_addition(self):
        arr1 = numpy.ones((128, 128)) * 0.1
        arr2 = numpy.ones((128, 128)) * 0.2
        res = numpy.ones((128, 128)) * 0.3

        terr1 = Terrain(array=arr1)
        terr2 = Terrain(array=arr2)
        terr_res = terr1 + terr2

        self.assertTrue(numpy.isclose(res, terr_res.to_array()).all())

    def test_terrain_subtraction(self):
        arr1 = numpy.ones((128, 128)) * 0.3
        arr2 = numpy.ones((128, 128)) * 0.2
        res = numpy.ones((128, 128)) * 0.1

        terr1 = Terrain(array=arr1)
        terr2 = Terrain(array=arr2)
        terr_res = terr1 - terr2

        self.assertTrue(numpy.isclose(res, terr_res.to_array()).all())

    def test_terrain_multiplication(self):
        arr1 = numpy.ones((128, 128)) * 2.0
        arr2 = numpy.ones((128, 128)) * .5
        res = numpy.ones((128, 128))

        terr1 = Terrain(array=arr1)
        terr2 = Terrain(array=arr2)
        terr_res = (terr1 * terr2)

        self.assertTrue(numpy.isclose(res, terr_res.to_array()).all())

    def test_terrain_division(self):
        arr1 = numpy.ones((128, 128)) * 2.0
        arr2 = numpy.ones((128, 128)) * .5
        res = numpy.ones((128, 128)) * 0.25

        terr1 = Terrain(array=arr1)
        terr2 = Terrain(array=arr2)
        terr_res = terr2 / terr1

        self.assertTrue(numpy.isclose(res, terr_res.to_array()).all())

    def test_terrain_copy_array(self):
        arr = numpy.arange(0, 16 * 16).reshape((16, 16))
        terr = Terrain(array=arr)

        copy_arr = terr.to_array()  # type: numpy.ndarray
        self.assertTrue(numpy.equal(arr, copy_arr).all())

        copy_arr[::2] = 0
        self.assertFalse(numpy.equal(arr, copy_arr).all())

    def test_terrain_normalize(self):
        arr = numpy.random.uniform(-100, 1000, (512, 512))
        terr = Terrain(array=arr)

        normalized = terr.to_normalized_array()
        norm_min = numpy.min(normalized)
        norm_max = numpy.max(normalized)

        self.assertTupleEqual((norm_min, norm_max), (0, 1))

    def test_terrain_denormalize(self):
        self._test_denormalize(-200, 500)
        self._test_denormalize(0, 300)
        self._test_denormalize(200, 250)

    def test_terrain_denormalize_reverse(self):
        exp_min, exp_max = 200, 450
        arr = numpy.random.uniform(size=(512, 512))
        terr = Terrain.from_normalized(arr, exp_max, exp_min)
        self.assertTupleEqual((exp_min, exp_max), tuple(map(lambda x: int(round(x)), terr.get_min_max_altitude())))

    def test_terrain_to_vectors(self):
        arr = numpy.zeros((32, 32))
        terr = Terrain(array=arr)
        vecs = terr.to_vectors()

        expected_vecs = numpy.reshape(numpy.array(list([i, j, 0.0]
                                                       for i, j in product(range(32), repeat=2))), (32, 32, 3))
        self.assertEqual(vecs.shape, (32, 32, 3))
        self.assertTrue(numpy.equal(expected_vecs, vecs).all())

    def test_terrain_get_normals(self):
        terr = Terrain(array=numpy.zeros((32, 32)))
        normals = terr.get_face_normals()

        expected_normals = numpy.reshape(numpy.array(list([0., 0., 1.] for _ in range(32 * 32))), (32, 32, 3))

        self.assertEqual(normals.shape, (32, 32, 3))
        self.assertTrue(numpy.equal(expected_normals, normals).all())

    def _test_denormalize(self, exp_min, exp_max):
        arr = numpy.random.uniform(size=(512, 512))
        terr = Terrain.from_normalized(arr, exp_min, exp_max)
        self.assertTupleEqual((exp_min, exp_max), tuple(map(lambda x: int(round(x)), terr.get_min_max_altitude())))


if __name__ == '__main__':
    unittest.main()

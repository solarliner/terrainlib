from unittest.mock import Mock

from terrainlib.filters.base import TerrainFilter
from terrainlib.terrain import Terrain


class NoopTerrainFilter(TerrainFilter):

    def __init__(self):
        self.mock_yourself = Mock()

    def __call__(self, terrain: Terrain):
        self.mock_yourself()
        return terrain

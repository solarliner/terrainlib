"""Operators unit tests."""
# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import unittest
from typing import Optional

import numpy

from terrainlib.operators.func import function_operator
from terrainlib.terrain import Terrain


class TerrainOperatorTestCase(unittest.TestCase):
    def test_lambda_works_sum(self):
        terrain0 = Terrain(array=numpy.zeros((512, 512)))
        terrain1 = Terrain(array=numpy.ones((512, 512)))
        terrain_result = terrain1.copy()

        @function_operator
        def add(*terrains: Terrain):
            result = None  # type: Optional[Terrain]
            for terrain in terrains:
                if result is None:
                    result = terrain.copy()
                else:
                    result += terrain
            return result

        self.assertEqual(add(terrain0, terrain1), terrain_result)

    def test_lambda_works_mult(self):
        terrain0 = Terrain(array=numpy.zeros((512, 512)))
        terrain1 = Terrain(array=numpy.ones((512, 512)))
        terrain_result = terrain0.copy()

        @function_operator
        def add(*terrains: Terrain):
            result = None  # type: Optional[Terrain]
            for terrain in terrains:
                if result is None:
                    result = terrain.copy()
                else:
                    result *= terrain
            return result

        self.assertEqual(add(terrain0, terrain1), terrain_result)


if __name__ == '__main__':
    unittest.main()

"""Unit tests for the TerrainLib project."""
# TerrainLib - A fast terrain generation library
# Copyright © 2018  Nathan "SolarLiner" Graule

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import unittest
from typing import Tuple

from terrainlib.filters import TerrainFilter
from terrainlib.generators.base import TerrainGenerator
from terrainlib.generators.procedural import DiamondSquareGenerator
from terrainlib.terrain import Terrain


class Base:
    class GeneratorTest(unittest.TestCase):

        def get_generator(self) -> Tuple[TerrainGenerator, int]:
            raise NotImplementedError()

        def generate(self):
            return self.gen()

        def setUp(self):
            self.gen, self.size = self.get_generator()
            self.terrain = self.generate()

        def test_generates(self):
            self.assertIsInstance(self.terrain, Terrain)

        def test_size_complies(self):
            self.assertEqual(self.terrain.size, self.size)

    class FilterTest(unittest.TestCase):
        filter: TerrainFilter = None

        def setUp(self):
            self.terrain = DiamondSquareGenerator(5, 0.1)()
            self.filtered = self.filter(self.terrain)

        def test_terrain_same_size(self):
            self.assertEqual(self.terrain.size, self.filtered.size)


def test_suite():
    loader = unittest.defaultTestLoader
    return loader.discover("tests", pattern="*_test.py")

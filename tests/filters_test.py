import unittest

from terrainlib.filters.erosion import SharpTerraceErosionFilter, SmoothTerraceErosionFilter, ThermalErosionFilter
from tests import Base


class ThermalErosionTest(Base.FilterTest):
    filter = ThermalErosionFilter(10)


class SharpTerraceErosionTest(Base.FilterTest):
    filter = SharpTerraceErosionFilter(.5)


class SmoothTerraceErosionTest(Base.FilterTest):
    filter = SmoothTerraceErosionFilter()


if __name__ == '__main__':
    unittest.main()

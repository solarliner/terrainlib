import random
import unittest

import numpy

from terrainlib.generators.procedural import (
    ConstantGenerator, DiamondSquareGenerator, OpenSimplexGenerator,
    VoronoiGenerator,
)
from tests import Base


class TestDiamondSquareGenerator(Base.GeneratorTest):

    def get_generator(self):
        power = 6
        size = 2 ** power + 1
        return DiamondSquareGenerator(power, 0.1), size

    def test_same_seed(self):
        for i in range(5):
            seed = random.random()
            with self.subTest(i=i, seed=seed):
                gen1 = DiamondSquareGenerator(6, 0.1, seed)
                gen2 = DiamondSquareGenerator(6, 0.1, seed)

                self.assertTrue(numpy.allclose(gen1().to_array(), gen2().to_array()))

    def test_different_seeds(self):
        for i in range(5):
            seed1 = random.random()
            seed2 = random.random()

            with self.subTest(i=i, seed1=seed1, seed2=seed2):
                gen1 = DiamondSquareGenerator(6, 0.1, seed1)
                gen2 = DiamondSquareGenerator(6, 0.1, seed2)

                self.assertFalse(numpy.allclose(gen1().to_array(), gen2().to_array()))


class TestVoronoiGenerator(Base.GeneratorTest):

    def get_generator(self):
        size = 1024
        return VoronoiGenerator(size), size

    def generate(self):
        points = numpy.random.uniform(0, self.size, (50, 2))
        return self.gen(points)


class TestConstantGenerator(Base.GeneratorTest):
    def get_generator(self):
        size = 1024
        self.height = 50
        return ConstantGenerator(size, self.height), size

    def test_height(self):
        self.assertTrue(numpy.equal(self.terrain.to_array(), self.height).all())


class TestOpenSimplexGenerator(Base.GeneratorTest):

    def get_generator(self):
        size = 512

        return OpenSimplexGenerator(size), size


if __name__ == '__main__':
    unittest.main()

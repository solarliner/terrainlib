import unittest
from collections import OrderedDict
from typing import Optional

import numpy
from PIL import Image

from terrainlib.filters.erosion import SharpTerraceErosionFilter, SmoothTerraceErosionFilter, ThermalErosionFilter
from terrainlib.generators.procedural import DiamondSquareGenerator
from terrainlib.operators.func import function_operator
from terrainlib.readers.image import PILImageReader
from terrainlib.runners.graph import GraphTerrainRunner
from terrainlib.runners.list import ListTerrainRunner
from terrainlib.terrain import Terrain
from tests.utils import NoopTerrainFilter


class ListRunnerTestCase(unittest.TestCase):
    def test_works(self):
        generator = DiamondSquareGenerator(8, 0.2)
        terrain = generator()

        erosion_filter = ThermalErosionFilter(50)
        terrace_filter = SharpTerraceErosionFilter(0.3)
        transforms_chain = [
            (1, erosion_filter),
            (2, terrace_filter)
        ]

        result = ListTerrainRunner(OrderedDict(transforms_chain)).run(terrain)
        self.assertTrue(isinstance(result, Terrain))
        expected_terrain = terrace_filter(erosion_filter(terrain))
        self.assertTrue(numpy.equal(result.to_array(), expected_terrain.to_array()).all())

    def test_sublist_on_list(self):
        noop_filter = NoopTerrainFilter()
        transform_chain_inner = [
            (1, SmoothTerraceErosionFilter()),
            (2, SharpTerraceErosionFilter(0.2)),
            (3, noop_filter)
        ]
        runner_inner = ListTerrainRunner(OrderedDict(transform_chain_inner))

        transform_chain = [
            (1, DiamondSquareGenerator(10, 0.1)),
            (2, runner_inner),
            (3, ThermalErosionFilter(50)),
            (4, PILImageReader(PILImageReader.BITDEPTH_8))
        ]
        runner = ListTerrainRunner(OrderedDict(transform_chain))
        result = runner.run()
        self.assertTrue(isinstance(result, Image.Image))
        self.assertTrue(noop_filter.mock_yourself.called)


class GraphTerrainRunnerTestCase(unittest.TestCase):
    @unittest.skip
    def test_works(self):
        height = 500

        @function_operator
        def sum_set_height(*terrains: Terrain):
            result = None  # type: Optional[Terrain]
            for terrain in terrains:
                if result is None:
                    result = terrain.copy()
                else:
                    result += terrain
            return Terrain(array=result.to_normalized_array() * height)

        graph = [
            DiamondSquareGenerator(8, 0.2),
            DiamondSquareGenerator(8, 0.4)
        ]

        runner = GraphTerrainRunner(graph, sum_set_height)
        result = runner.run()
        self.assertEqual(result.get_min_max_altitude()[1], height)


if __name__ == '__main__':
    unittest.main()

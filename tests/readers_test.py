import unittest
from pathlib import Path

from PIL.Image import Image

from terrainlib.generators.procedural import DiamondSquareGenerator
from terrainlib.readers.image import PILImageReader


class TestImageExport(unittest.TestCase):
    def setUp(self):
        self.terrain = DiamondSquareGenerator(6, .1)()
        self.file_png = Path("test_terrain_export.png")
        self.file_tif = Path("test_terrain_export.tif")

    def tearDown(self):
        if self.file_png.exists():
            self.file_png.unlink()
        if self.file_tif.exists():
            self.file_tif.unlink()

    def save_bitdepth_factory(self, bitdepth, filepath):
        reader = PILImageReader(bitdepth)
        img = reader(self.terrain)
        img.save(filepath)

    def test_outputs_pillow_img(self):
        reader = PILImageReader(PILImageReader.BITDEPTH_8)
        img = reader(DiamondSquareGenerator(6, .1)())
        self.assertIsInstance(img, Image)

    def test_save_8bit_png(self):
        self.save_bitdepth_factory(PILImageReader.BITDEPTH_8, self.file_png.resolve())
        self.assertTrue(self.file_png.is_file())

    @unittest.expectedFailure
    def test_save_16bit_png(self):
        self.save_bitdepth_factory(PILImageReader.BITDEPTH_16, self.file_png.resolve())
        self.assertTrue(self.file_png.is_file())

    def test_save_32bit_tif(self):
        self.save_bitdepth_factory(PILImageReader.BITDEPTH_32, self.file_tif.resolve())
        self.assertTrue(self.file_tif.is_file())

    def test_save_float_tif(self):
        self.save_bitdepth_factory(PILImageReader.BITDEPTH_FLOAT, self.file_tif.resolve())
        self.assertTrue(self.file_tif.is_file())

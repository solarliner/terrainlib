import logging
import sys
import time
from datetime import timedelta

from PIL import Image
import numpy

from terrainlib.terrain import Terrain
from terrainlib.generators.procedural import OpenSimplexGenerator
from terrainlib.readers.image import PILImageReader


class ElapsedFormatter(logging.Formatter):
    def __init__(self, fmt=None):
        super().__init__(fmt)
        self.start = time.time()

    def format(self, record):
        message = super(__class__, self).format(record)
        elapsed_time = timedelta(seconds=record.created - self.start)
        return '[{} s] {}'.format(elapsed_time, message)


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)


def setup():
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(logging.Formatter('[%(asctime)s] [%(levelname)8s] %(module)s: %(message)s'))
    sh.setLevel(logging.INFO)
    logger.addHandler(sh)
    fh = logging.FileHandler('debug.log', 'w')
    fh.setFormatter(ElapsedFormatter('%(levelname)s @ %(module)s: %(message)s'))
    fh.setLevel(logging.DEBUG)
    logger.addHandler(fh)


def main():
    generator = OpenSimplexGenerator(32, seed=42, ridged=False, octaves=8)
    # reader = PILImageReader(PILImageReader.BITDEPTH_8)

    # logger.info('Generating terrain...')
    # terrain = generator()
    # img = reader(terrain)
    # logger.info('Saving terrain...')
    # img.save('terrain_out.png')
    # print("Done.")

    # terr = Terrain(array=numpy.zeros((64, 64)))
    terr = generator()
    print(repr(terr.get_face_normals()))



if __name__ == '__main__':
    setup()
    main()

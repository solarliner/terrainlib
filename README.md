# ![TerrainLib](docs_src/_static/Type.png)

A fast landscape generation library that's modular and artist-first.

## Changelog

See [CHANGELOG.md](CHANGELOG.md) to lean about the novelties of each version.

## Use

Learning how the library is structured goes a long way in understanding how to 
operate the classes in this library. As such, you should read the documentation
(see below) in order to know more about the library itself and its uses.

Every transform (the generic word for code that applies in some way to a Terrain object) is first initialized, then
called upon. This allows a preparation step if needed, for the algorithms to bootstrap themselves.
Generators and filters output a Terrain object, and Filters and Readers take in
a Terrain object, so you can chain filters to achieve the desired effect.

Runners, in turn, abstract that whole process, allowing the use of lists of transforms, running the whole at once, and
giving out the final Terrain (or Reader output, should you have one at the end) object. The runner provides other 
niceties such as progress report.

The developer documentation is available at https://solarliner.gitlab.io/terrainlib.

## Development install

This project uses Pipenv to manage dev and production dependencies. Therefore, 
virtual environment creation and dependency installation is all done with a 
single command:
```bash
pipenv install --dev
```

The project uses [PEP 484](https://www.python.org/dev/peps/pep-0484/), using
static type checking as a first defense against bugs. The transition to typed
Python is still under way, so if you come across some untyped code, do type it!

## License

This project is licensed under a GNU LGPLv3 license. Non open-source, commercial
projects are welcomed, but should ask for explicit licensing agreement.  
See the [license file](LICENSE) for more information.
